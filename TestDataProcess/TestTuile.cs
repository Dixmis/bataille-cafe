﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TraitementDonnees__Data_Process_;

namespace TestDataProcess
{
    /// <summary>
    /// Description résumée pour TestTuile
    /// </summary>
    [TestClass]
    public class TestTuile
    {
        public TestTuile()
        {
            Tuile tuile = new Tuile(70);
            Assert.AreEqual(2, tuile.Type);
            Assert.AreEqual(true, tuile.FrontiereOuest);
            Assert.AreEqual(true, tuile.FrontiereSud);
            Assert.AreEqual(false, tuile.FrontiereEst);
            Assert.AreEqual(false, tuile.FrontiereNord);
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active, ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        //
        // Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        // Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test de la classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Utilisez ClassCleanup pour exécuter du code une fois que tous les tests d'une classe ont été exécutés
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestHashCodeTuile()
        {
            Assert.AreEqual(42, new Tuile(42).GetHashCode());
            Assert.AreEqual(70, new Tuile(70).GetHashCode());
            Assert.AreEqual(3, new Tuile(3).GetHashCode());
            Assert.AreEqual(8, new Tuile(8).GetHashCode());
            Assert.AreEqual(14, new Tuile(14).GetHashCode());
        }
    }
}
