﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TraitementDonnees__Data_Process_;

namespace TestDataProcess
{
    [TestClass]
    public class TestStringProcess
    {
        [TestMethod]
        public void TestString2TuileArray()
        {
            Tuile[,] map = new Tuile[10, 10];
            map[0, 0] = new Tuile(3);
            map[0, 1] = new Tuile(9);
            map[0, 2] = new Tuile(71);
            map[0, 3] = new Tuile(69);
            map[0, 4] = new Tuile(65);
            map[0, 5] = new Tuile(65);
            map[0, 6] = new Tuile(65);
            map[0, 7] = new Tuile(65);
            map[0, 8] = new Tuile(65);
            map[0, 9] = new Tuile(73);
            map[1, 0] = new Tuile(2);
            map[1, 1] = new Tuile(8);
            map[1, 2] = new Tuile(3);
            map[1, 3] = new Tuile(9);
            map[1, 4] = new Tuile(70);
            map[1, 5] = new Tuile(68);
            map[1, 6] = new Tuile(64);
            map[1, 7] = new Tuile(64);
            map[1, 8] = new Tuile(64);
            map[1, 9] = new Tuile(72);
            map[2, 0] = new Tuile(6);
            map[2, 1] = new Tuile(12);
            map[2, 2] = new Tuile(2);
            map[2, 3] = new Tuile(8);
            map[2, 4] = new Tuile(3);
            map[2, 5] = new Tuile(9);
            map[2, 6] = new Tuile(70);
            map[2, 7] = new Tuile(68);
            map[2, 8] = new Tuile(64);
            map[2, 9] = new Tuile(72);
            map[3, 0] = new Tuile(11);
            map[3, 1] = new Tuile(11);
            map[3, 2] = new Tuile(6);
            map[3, 3] = new Tuile(12);
            map[3, 4] = new Tuile(6);
            map[3, 5] = new Tuile(12);
            map[3, 6] = new Tuile(3);
            map[3, 7] = new Tuile(9);
            map[3, 8] = new Tuile(70);
            map[3, 9] = new Tuile(76);
            map[4, 0] = new Tuile(10);
            map[4, 1] = new Tuile(10);
            map[4, 2] = new Tuile(11);
            map[4, 3] = new Tuile(11);
            map[4, 4] = new Tuile(67);
            map[4, 5] = new Tuile(73);
            map[4, 6] = new Tuile(6);
            map[4, 7] = new Tuile(12);
            map[4, 8] = new Tuile(3);
            map[4, 9] = new Tuile(9);
            map[5, 0] = new Tuile(14);
            map[5, 1] = new Tuile(14);
            map[5, 2] = new Tuile(10);
            map[5, 3] = new Tuile(10);
            map[5, 4] = new Tuile(70);
            map[5, 5] = new Tuile(76);
            map[5, 6] = new Tuile(6);
            map[5, 7] = new Tuile(13);
            map[5, 8] = new Tuile(6);
            map[5, 9] = new Tuile(12);
            map[6, 0] = new Tuile(3);
            map[6, 1] = new Tuile(9);
            map[6, 2] = new Tuile(14);
            map[6, 3] = new Tuile(14);
            map[6, 4] = new Tuile(11);
            map[6, 5] = new Tuile(7);
            map[6, 6] = new Tuile(13);
            map[6, 7] = new Tuile(3);
            map[6, 8] = new Tuile(9);
            map[6, 9] = new Tuile(75);
            map[7, 0] = new Tuile(2);
            map[7, 1] = new Tuile(8);
            map[7, 2] = new Tuile(7);
            map[7, 3] = new Tuile(13);
            map[7, 4] = new Tuile(14);
            map[7, 5] = new Tuile(3);
            map[7, 6] = new Tuile(9);
            map[7, 7] = new Tuile(6);
            map[7, 8] = new Tuile(12);
            map[7, 9] = new Tuile(78);
            map[8, 0] = new Tuile(6);
            map[8, 1] = new Tuile(12);
            map[8, 2] = new Tuile(3);
            map[8, 3] = new Tuile(1);
            map[8, 4] = new Tuile(9);
            map[8, 5] = new Tuile(6);
            map[8, 6] = new Tuile(12);
            map[8, 7] = new Tuile(35);
            map[8, 8] = new Tuile(33);
            map[8, 9] = new Tuile(41);
            map[9, 0] = new Tuile(71);
            map[9, 1] = new Tuile(77);
            map[9, 2] = new Tuile(6);
            map[9, 3] = new Tuile(4);
            map[9, 4] = new Tuile(12);
            map[9, 5] = new Tuile(39);
            map[9, 6] = new Tuile(37);
            map[9, 7] = new Tuile(36);
            map[9, 8] = new Tuile(36);
            map[9, 9] = new Tuile(44);
            Tuile[,] map2Check = StringProcess.String2TuileArray("3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44|");
            bool checkTableau = true;
            for (int i = 0; i < 10; i++)
            {
                if (!checkTableau) break;
                for (int j = 0; j < 10; j++)
                    if (map[i, j] == map2Check[i, j])
                    {
                        checkTableau = false;
                        break;
                    }
            }
            Assert.IsTrue(checkTableau);
        }
    }
}
