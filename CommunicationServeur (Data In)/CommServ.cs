﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;


namespace CommunicationServeur__Data_In_
{
    public class CommServ
    {
        Socket socket = null;
        byte[] received;
        byte[] toSend;
        bool local;

        public CommServ(string host, int port)
        {
            if (host != "localhost")
            {
                local = false;
                try
                {
                    socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    socket.Connect(host, port);
                }
                catch (SocketException)
                {
                    throw;
                }
            } else
            {
                local = true;
            }
        }
        public CommServ(string host) : this(host, 1212) { }
        public CommServ() : this("51.91.120.237", 1212) { }
        public string getFromServ(int length = 4)
        {
            if (local) return "3:9:71:69:65:65:65:65:65:73|2:8:3:9:70:68:64:64:64:72|6:12:2:8:3:9:70:68:64:72|11:11:6:12:6:12:3:9:70:76|10:10:11:11:67:73:6:12:3:9|14:14:10:10:70:76:7:13:6:12|3:9:14:14:11:7:13:3:9:75|2:8:7:13:14:3:9:6:12:78|6:12:3:1:9:6:12:35:33:41|71:77:6:4:12:39:37:36:36:44|";
            received = new byte[length];
            socket.Receive(received);
            return Encoding.ASCII.GetString(received);
        }
        public void sendToServ(string entry)
        {
            toSend = new byte[entry.Length];
            toSend = Encoding.ASCII.GetBytes(entry);
            socket.Send(toSend);
        }
        ~CommServ() {
            if (!local)
            {
                try
                {
                    socket.Shutdown(SocketShutdown.Both);
                }
                catch (SocketException exce)
                {
                    Console.WriteLine(exce.Message);
                    Console.WriteLine(String.Format("SocketErrorCode = {0}", exce.SocketErrorCode));
                }
                socket.Close();
            }
        }
    }
}
