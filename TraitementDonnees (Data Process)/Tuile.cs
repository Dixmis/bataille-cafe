﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraitementDonnees__Data_Process_
{
    public class Tuile
    {
        /*** Attributs ***/
        private int type;
        private int graine;

        /*** Propriétés ***/
        public bool FrontiereNord { get; set; }
        public bool FrontiereSud { get; set; }
        public bool FrontiereOuest { get; set; }
        public bool FrontiereEst { get; set; }
        public int Type
        {
            get => this.type;
            set
            {
                if (value < 0 || value > 2) this.type = 0;
                else this.type = value;
            }
        }
        public int Graine // 0 : personne ; 1 : serveur ; 2 : joueur
        {
            get => this.graine;
            set
            {
                if (value < 0 || value > 2) this.graine = 0;
                else this.graine = value;
            }
        }

        /*** Constructeur ***/
        public Tuile(int type, bool fNord, bool fOuest, bool fSud, bool fEst, int graine)
        {
            this.Type = type;
            this.FrontiereNord = fNord;
            this.FrontiereOuest = fOuest;
            this.FrontiereSud = fSud;
            this.FrontiereEst = fEst;
            this.Graine = graine;
        }
        public Tuile(int binaryIn)
        {
            initAttributs(binaryIn);
        }
        public Tuile(string binaryIn)
        {
            int binaryOut = 0;
            int.TryParse(binaryIn, out binaryOut);
            initAttributs(binaryOut);
        }
        public Tuile() : this(0, true, true, true, true, 0) { }

        /*** Méthodes ***/
        private void initAttributs(int binaryIn)
        {
            if (binaryIn - 64 >= 0)
            {
                this.Type = 2;
                binaryIn -= 64;
            }
            else if (binaryIn - 32 >= 0)
            {
                this.Type = 1;
                binaryIn -= 32;
            }
            else
            {
                this.Type = 0;
            }
            if ((binaryIn / 8) > 0)
            {
                binaryIn %= 8;
                this.FrontiereEst = true;
            }
            if ((binaryIn / 4) > 0)
            {
                binaryIn %= 4;
                this.FrontiereSud = true;
            }
            if ((binaryIn / 2) > 0)
            {
                binaryIn %= 2;
                this.FrontiereOuest = true;
            }
            if (binaryIn == 1)
            {
                this.FrontiereNord = true;
            }
            this.Graine = 0;
        }
        // override object.Equals
        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //
            
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Tuile o = (Tuile)obj;
            return (GetHashCode() == o.GetHashCode());
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            int hashCode = 0;
            if (FrontiereNord) hashCode += 1;
            if (FrontiereOuest) hashCode += 2;
            if (FrontiereSud) hashCode += 4;
            if (FrontiereEst) hashCode += 8;
            hashCode += this.type * 32;
            return hashCode;
        }
    }
}
