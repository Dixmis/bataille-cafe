﻿using CommunicationServeur__Data_In_;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraitementDonnees__Data_Process_
{
    public class JeuProcess
    {
        public Map Map { get; private set; }
        public CommServ Serv { get; private set; }
        public int[] NbrGraines { get; private set; }
        public int[] Scores { get; private set; }
        public bool InGame { get; private set; }
        public int[] LastPlayCoords { get; private set; }
        private bool FirstPlay { get; set; }
        public JeuProcess(Map map, CommServ serv)
        {
            this.Map = map;
            this.Serv = serv;
            NbrGraines = new int[] { 28, 28 };
            Scores = new int[] { 0, 0 };
            InGame = true;
            LastPlayCoords = new int[3];
            FirstPlay = true;
        }
        private void PlayerPlaceGraine(int x, int y)
        {
            Map.PlaceGraine(x, y, 2);
            Serv.sendToServ("A:" + x + y);
            NbrGraines[1]--;
        }
        private void PlayerPlaceGraine(int[] coords)
        {
            if (coords.Length == 2) PlayerPlaceGraine(coords[0], coords[1]);
        }
        public void Play()
        {
            if (FirstPlay)
            {
                Parcelle parcelle = TryFindParcelles3()[1];
                PlayerPlaceGraine(parcelle.coordsTuiles[1, 0], parcelle.coordsTuiles[1, 1]);
                FirstPlay = false;
                IPlayed();
                return;
            }
            else if (Step1())
            {
                IPlayed();
                return;
            }
            else if (Step2())
            {
                IPlayed();
                return;
            }
            else if (Step3())
            {
                IPlayed();
                return;
            }
            else if (Step4())
            {
                IPlayed();
                return;
            }
            else if (Step5())
            {
                IPlayed();
                return;
            }
        }
        private bool Step1()
        {
            return FindGoodPlacementInParcelles(TryFindParcelles3());
        }
        private bool Step2()
        {
            return FindGoodPlacementInParcelles(TryFindParcellesOwnByUs());
        }
        private bool Step3()
        {
            int[] coords = FindGoodPlacementWithOurGrainClose();
            if (coords != null && coords.Length == 2)
            {
                PlayerPlaceGraine(coords);
                return true;
            }
            else return false;
        }
        private bool Step4()
        {
            return FindGoodPlacementInParcelles(TryFindParcellesEmpty());
        }
        private bool Step5()
        {
            return FindGoodPlacementInParcelles(OrderParcellesFromBigToSmall());
        }
        private void IPlayed()
        {
            Serv.getFromServ();
            string serverPlay = Serv.getFromServ();
            if (serverPlay == "FINI")
            {
                EndOfGame();
                return;
            }
            string xCoor = serverPlay.Substring(2, 1);
            string yCoor = serverPlay.Substring(3);
            int.TryParse(xCoor, out int x);
            int.TryParse(yCoor, out int y);
            LastPlayCoords = new int[] { x, y, Map.IdsParcelles[x, y] };
            Map.PlaceGraine(x, y, 1);
            NbrGraines[0]--;
            string returned = Serv.getFromServ();
            if (returned == "FINI") EndOfGame();
            CalculPoints();
        }
        private void EndOfGame()
        {
            string returned = Serv.getFromServ(7);
            string[] scores = returned.Split(':');
            int.TryParse(scores[1], out Scores[1]);
            int.TryParse(scores[2], out Scores[0]);
            InGame = false;
        }
        private void CalculPoints()
        {
            Scores = new int[] { 0, 0 };
            foreach (Parcelle parcelle in Map.Parcelles)
            {
                if (parcelle.Owner > 0) Scores[parcelle.Owner - 1] += parcelle.Length;
            }
            int serveur_max_graines = 0, joueur_max_graines = 0;
            for (int x = 0; x < 10; x++)
                for (int y = 0; y < 10; y++)
                {
                    if (Map.Plateau[x, y].Graine != 0)
                    {
                        int current_max_graines = 0;
                        bool[,] graineChecked = new bool[10, 10];
                        for (int i = 0; i < 10; i++) for (int j = 0; j < 10; j++) graineChecked[i, j] = false;
                        if (Map.Plateau[x, y].Graine == 1) CheckAroundGraine(x, y, 1, ref current_max_graines, ref serveur_max_graines, ref graineChecked);
                        else if (Map.Plateau[x, y].Graine == 2) CheckAroundGraine(x, y, 2, ref current_max_graines, ref joueur_max_graines, ref graineChecked);
                    }
                }
            Scores[0] += serveur_max_graines;
            Scores[1] += joueur_max_graines;
        }
        private void CheckAroundGraine(int x, int y, int owner, ref int current_max_graines, ref int global_max_graines, ref bool[,] graineChecked)
        {
            if (!graineChecked[x, y])
            {
                current_max_graines++;
                graineChecked[x, y] = true;
                if (global_max_graines < current_max_graines) global_max_graines = current_max_graines;
            } else { return; }
            if (x + 1 < 10) if (Map.Plateau[x + 1, y].Graine == owner) CheckAroundGraine(x + 1, y, owner, ref current_max_graines, ref global_max_graines, ref graineChecked);
            if (x - 1 >= 0) if (Map.Plateau[x - 1, y].Graine == owner) CheckAroundGraine(x - 1, y, owner, ref current_max_graines, ref global_max_graines, ref graineChecked);
            if (y + 1 < 10) if (Map.Plateau[x, y + 1].Graine == owner) CheckAroundGraine(x, y + 1, owner, ref current_max_graines, ref global_max_graines, ref graineChecked);
            if (y - 1 >= 0) if (Map.Plateau[x, y - 1].Graine == owner) CheckAroundGraine(x, y - 1, owner, ref current_max_graines, ref global_max_graines, ref graineChecked);
        }
        private Parcelle[] TryFindParcelles3()
        {
            List<Parcelle> parcelles = new List<Parcelle>();
            foreach (Parcelle parcelle in Map.Parcelles) { 
                if (parcelle.Length == 3 && parcelle.IsEmpty)
                {
                    parcelles.Add(parcelle);
                }
            }
            return parcelles.ToArray();
        }
        private Parcelle[] TryFindParcellesOwnByUs()
        {
            List<Parcelle> parcelles = new List<Parcelle>();
            foreach (Parcelle parcelle in Map.Parcelles)
            {
                if (parcelle.Owner == 1 && parcelle.Filled < parcelle.Length)
                {
                    parcelles.Add(parcelle);
                }
            }
            return parcelles.ToArray();
        }
        private Parcelle[] TryFindParcellesEmpty()
        {
            List<Parcelle> parcelles = new List<Parcelle>();
            foreach (Parcelle parcelle in Map.Parcelles)
            {
                if (parcelle.IsEmpty)
                {
                    parcelles.Add(parcelle);
                }
            }
            return parcelles.ToArray();
        }
        private Parcelle[] OrderParcellesFromBigToSmall()
        {
            List<Parcelle> parcelles = new List<Parcelle>();
            foreach (Parcelle parcelle in Map.Parcelles)
                if (parcelle.Length == 6)
                    parcelles.Add(parcelle);
            foreach (Parcelle parcelle in Map.Parcelles)
                if (parcelle.Length == 4)
                    parcelles.Add(parcelle);
            foreach (Parcelle parcelle in Map.Parcelles)
                if (parcelle.Length == 3)
                    parcelles.Add(parcelle);
            foreach (Parcelle parcelle in Map.Parcelles)
                if (parcelle.Length == 2)
                    parcelles.Add(parcelle);
            return parcelles.ToArray();
        }
        private int[] FindGoodPlacementWithOurGrainClose()
        {
            int x, y;
            bool found = false;
            for (x = 0; x < 10; x++)
            {
                if (IsGoodPlacement(x, LastPlayCoords[1]) && IsOurGraineClose(x, LastPlayCoords[1]))
                {

                    found = true;
                    break;
                }
            }
            if (found) return new int[] { x, LastPlayCoords[1] };
            for (y = 0; y < 10; y++)
            {
                if (IsGoodPlacement(LastPlayCoords[0], y) && IsOurGraineClose(LastPlayCoords[0], y))
                {
                    found = true;
                    break;
                }
            }
            if (found) return new int[] { LastPlayCoords[0], y };
            else return null;
        }
        private int[] FindGoodPlacementInParcelle(Parcelle parcelle)
        {
            for (int j = 0; j < parcelle.Length; j++)
            {
                if (IsGoodPlacement(parcelle.coordsTuiles[j, 0], parcelle.coordsTuiles[j, 1]))
                {
                    int[] coords = new int[2];
                    coords[0] = parcelle.coordsTuiles[j, 0];
                    coords[1] = parcelle.coordsTuiles[j, 1];
                    return coords;
                }
            }
            return null;
        }
        private bool FindGoodPlacementInParcelles(Parcelle[] parcelles)
        {
            if (parcelles.Length > 0)
            {
                foreach (Parcelle parcelle in parcelles)
                {
                    int[] coords = FindGoodPlacementInParcelle(parcelle);
                    if (coords != null && coords.Length == 2)
                    {
                        PlayerPlaceGraine(coords);
                        return true;
                    }
                }
                return false;
            }
            else return false;
        }
        private bool IsGoodPlacement(int x, int y, int id)
        {
            if (LastPlayCoords[0] != x && LastPlayCoords[1] != y) return false;
            if (LastPlayCoords[2] == id) return false;
            if (LastPlayCoords[0] == x && LastPlayCoords[1] == y) return false;
            if (Map.Plateau[x, y].Graine != 0) return false;
            if (Map.Plateau[x, y].Type != 0) return false;
            if (x < 0 || x >= 10 || y < 0 || y >= 10) return false;
            return true;
        }
        private bool IsGoodPlacement(int x, int y)
        {
            return IsGoodPlacement(x, y, Map.IdsParcelles[x, y]);
        }
        private bool IsOurGraineClose(int x, int y)
        {
            if (x + 1 < 10) if (Map.Plateau[x + 1, y].Graine == 2) return true;
            if (x - 1 >= 0) if (Map.Plateau[x - 1, y].Graine == 2) return true;
            if (y + 1 < 10) if (Map.Plateau[x, y + 1].Graine == 2) return true;
            if (y - 1 >= 0) if (Map.Plateau[x, y - 1].Graine == 2) return true;
            return false;
        }
    }
}
