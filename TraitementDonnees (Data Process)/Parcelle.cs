﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraitementDonnees__Data_Process_
{
    public class Parcelle
    {
        /** Attributs **/
        private readonly int ID;
        /** Propriétés **/
        public int Length
        {
            get
            {
                return Tuiles.Count;
            }
        }
        public int Owner { get; private set; }
        public List<Tuile> Tuiles { get; private set; }
        public int[,] coordsTuiles { get; private set; }
        public bool IsEmpty
        {
            get
            {
                foreach(Tuile tuile in Tuiles)
                {
                    if (tuile.Graine != 0) return false;
                }
                return true;
            }
        }
        public int Filled
        {
            get
            {
                int count = 0;
                foreach (Tuile tuile in Tuiles)
                {
                    if (tuile.Graine != 0) count++;
                }
                return count;
            }
        }
        /** Constructeur **/
        public Parcelle(Map map, int id)
        {
            ID = id;
            Tuiles = new List<Tuile>(6);
            coordsTuiles = new int[6,2];
            int i = -1, j = -1;
            for (int x=0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    if (map.Plateau[x, y].Type == 0 && map.IdsParcelles[x, y] == -1)
                    {
                        i = x;
                        j = y;
                        break;
                    }
                    if (i >= 0 && j >= 0) break;
                }
                if (i >= 0 && j >= 0) break;
            }
            CheckAroundTuile(i, j, map);
        }
        /** Méthodes **/
        private void CheckAroundTuile(int x, int y, Map map)
        {
            Tuile currentTuile = map.Plateau[x, y];
            if (map.IdsParcelles[x, y] == ID) return;
            map.IdsParcelles[x, y] = ID;
            Tuiles.Add(currentTuile);
            coordsTuiles[Length - 1, 0] = x;
            coordsTuiles[Length - 1, 1] = y;
            if (!(currentTuile.FrontiereSud)) if (x + 1 < 10) CheckAroundTuile(x + 1, y, map);
            if (!(currentTuile.FrontiereNord)) if (x - 1 >= 0) CheckAroundTuile(x - 1, y, map);
            if (!(currentTuile.FrontiereEst)) if (y + 1 < 10) CheckAroundTuile(x, y + 1, map);
            if (!(currentTuile.FrontiereOuest)) if (y - 1 >= 0) CheckAroundTuile(x, y - 1, map);
        }
        public int CheckOwner()
        {
            int countN = 0, countJ = 0, countS = 0, res = 0;
            for (int i = 0; i < this.Length; i++)
            {
                if (Tuiles[i].Graine == 1) countS++;
                else if (Tuiles[i].Graine == 2) countJ++;
                else countN++;
            }
            if (countS > countJ) res = 1;
            else if (countJ > countS) res = 2;
            else res = 0;
            Owner = res;
            return res;
        }
    }
}
