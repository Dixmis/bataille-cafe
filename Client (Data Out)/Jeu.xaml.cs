﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TraitementDonnees__Data_Process_;
using CommunicationServeur__Data_In_;

namespace Client__Data_Out_
{
    /// <summary>
    /// Logique d'interaction pour Jeu.xaml
    /// </summary>
    public partial class Jeu : Window
    {
        private CommServ serv;
        private Map map;
        private Label[,] lblTuile;
        private string host;
        private int port;
        private JeuProcess maingame;
        public Jeu(string host, int port)
        {
            this.host = host;
            this.port = port;
            InitializeComponent();
            try
            {
                serv = new CommServ(host, port);
            }
            catch (Exception exce)
            {
                throw exce;
            }
            lblTuile = new Label[10, 10];
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    lblTuile[i, j] = new Label();
                }
        }
        public Jeu() : this("51.91.120.237", 1212) { }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double height = (this.Height - firstRow.Height.Value - lastRow.Height.Value) / 10 - 12, width = this.Width / 10 - 12, size;
            if (height > width) size = width;
            else size = height;
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                    lblTuile[i, j].Width = lblTuile[i, j].Height = size;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            //sldVitesse.Width = window.Width * 0.10;
            string mapString = serv.getFromServ(300);
            map = new Map(mapString);
            maingame = new JeuProcess(map, serv);
            double height = (this.Height - lastRow.Height.Value - firstRow.Height.Value) / 10 - 12, width = this.Width / 10 - 12, size;
            if (height > width) size = width;
            else size = height;
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    gridPlateau.Children.Add(lblTuile[i, j]);
                    Grid.SetColumn(lblTuile[i, j], j);
                    Grid.SetRow(lblTuile[i, j], i);
                    lblTuile[i, j].HorizontalContentAlignment = HorizontalAlignment.Center;
                    lblTuile[i, j].VerticalContentAlignment = VerticalAlignment.Center;
                    lblTuile[i, j].Padding = new Thickness(0);
                    //if (map.IdsParcelles[i, j] >= 0) lblTuile[i, j].Content = (char)(map.IdsParcelles[i, j] + 'A');
                    lblTuile[i, j].Width = lblTuile[i, j].Height = size;
                    if (map.Plateau[i, j].Type == 2)
                    {
                        //lblTuile[i, j].Background = Brushes.Blue;
                        lblTuile[i, j].Content = new Image
                        {
                            Source = new BitmapImage(new Uri("/img/mer2.png", UriKind.Relative)),
                            Stretch = Stretch.Fill
                        };
                    }
                    else if (map.Plateau[i, j].Type == 1)
                    {
                        lblTuile[i, j].Background = Brushes.ForestGreen;
                        lblTuile[i, j].Content = new Image
                        {
                            Source = new BitmapImage(new Uri("/img/arbre.png", UriKind.Relative)),
                            Stretch = Stretch.UniformToFill
                        };
                    }
                    else
                    {
                        lblTuile[i, j].Background = Brushes.LawnGreen;
                        lblTuile[i, j].BorderBrush = Brushes.Black;
                        lblTuile[i, j].BorderThickness = new Thickness(Convert.ToDouble(map.Plateau[i, j].FrontiereOuest), Convert.ToDouble(map.Plateau[i, j].FrontiereNord), Convert.ToDouble(map.Plateau[i, j].FrontiereEst), Convert.ToDouble(map.Plateau[i, j].FrontiereSud));
                    }
                }
            //lblStatus.Content = lblStatus.Content + " " + mapString;
        }

        private void btnDeco_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow(host, port);
            window.Show();
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            serv = null;
        }

        private void btnNextStep_Click(object sender, RoutedEventArgs e)
        {
            maingame.Play();
            Actualiser();
        }
        private void Actualiser()
        {
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    if (map.Plateau[x, y].Type == 0)
                    {
                        if (map.Plateau[x, y].Graine == 1) lblTuile[x, y].Content = new Image
                        {
                            Source = new BitmapImage(new Uri("/img/grain_cafe1.png", UriKind.Relative)),
                            Stretch = Stretch.UniformToFill
                        };
                        if (map.Plateau[x, y].Graine == 2) lblTuile[x, y].Content = new Image
                        {
                            Source = new BitmapImage(new Uri("/img/grain_cafe.png", UriKind.Relative)),
                            Stretch = Stretch.UniformToFill
                        };
                    }
                }
            }
            lblNbGraineAd.Content = maingame.NbrGraines[0];
            lblNbGraineIA.Content = maingame.NbrGraines[1];
            lblPointsAd.Content = maingame.Scores[0];
            lblPointsIA.Content = maingame.Scores[1];
            if (!maingame.InGame)
            {
                btnNextStep.Visibility = Visibility.Hidden;
                serv = null;
            }
        }
    }
}
