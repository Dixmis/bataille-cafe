﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TraitementDonnees__Data_Process_;
using CommunicationServeur__Data_In_;
using System.Net.Sockets;

namespace Client__Data_Out_
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow() : this("127.0.0.1", 1213) { }
        public MainWindow(string host, int port)
        {
            InitializeComponent();
            tbxIPHost.Text = host;
            tbxPortHost.Text = port.ToString();
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            Jeu jeu;
            try
            {
                jeu = new Jeu(tbxIPHost.Text, Convert.ToInt32(tbxPortHost.Text));
            }
            catch (Exception exce)
            {
                MessageBox.Show(exce.Message, "Erreur de connexion");
                return;
            }
            jeu.Show();
            this.Close();
        }

        private void btnConnectServ0_Click(object sender, RoutedEventArgs e)
        {
            Jeu jeu;
            try
            {
                jeu = new Jeu("51.91.120.237", 1212);
            }
            catch (Exception exce)
            {
                MessageBox.Show(exce.Message, "Erreur de connexion");
                return;
            }
            jeu.Show();
            this.Close();
        }

        private void btnConnectServ1_Click(object sender, RoutedEventArgs e)
        {
            Jeu jeu;
            try
            {
                jeu = new Jeu("51.91.120.237", 1213);
            }
            catch (Exception exce)
            {
                MessageBox.Show(exce.Message, "Erreur de connexion");
                return;
            }
            jeu.Show();
            this.Close();
        }

        private void btnConnectLocal_Click(object sender, RoutedEventArgs e)
        {
            Jeu jeu;
            try
            {
                jeu = new Jeu("localhost", 1212);
            }
            catch (Exception exce)
            {
                MessageBox.Show(exce.Message, "Erreur de connexion");
                return;
            }
            jeu.Show();
            this.Close();
        }
    }
}
